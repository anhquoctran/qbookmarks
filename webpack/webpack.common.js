const webpack = require("webpack");
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const srcDir = '../src/modules/';

module.exports = {
  entry: {
    items: path.join(__dirname, srcDir + 'items.ts'),
    popup: path.join(__dirname, srcDir + 'popup.ts'),
    options: path.join(__dirname, srcDir + 'options.ts'),
    background: path.join(__dirname, srcDir + 'background.ts'),
    contentscript: path.join(__dirname, srcDir + 'contentscript.ts'),
  },
  output: {
    path: path.join(__dirname, '../dist/js'),
    filename: '[name].js'
  },
  optimization: {
    splitChunks: {
      name: 'vendor',
      chunks: "initial"
    }
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  plugins: [
    // exclude locale files in moment
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new CopyPlugin([{
      from: '.',
      to: '../'
    }], {
      context: './src/views'
    }),
    new CopyPlugin([{
      from: './manifest.json',
      to: '../manifest.json'
    }], {
      context: './src'
    }),
    new CopyPlugin([
      {
        from: './images',
        to: '../images'
      }
    ], { context: './src'})
  ]
};