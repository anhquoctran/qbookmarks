export interface SearchItem {
  id: string,
  title: string,
  description?: string,
  url: string,
  created?: Date
}