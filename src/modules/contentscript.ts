import { SearchItem } from './items';

function searchBookmarks(searchString: string = '') {
  let results: Array<SearchItem> = [];
  if(searchString) {
    
    chrome.bookmarks.search(searchString, function(result) {
      results = result.map(function(item, index) {
        const it: SearchItem = {
          id: item.id,
          created: new Date(),
          title: item.title,
          url: item.url
        };
        return it;
      });
      
    });
  }
}